'use strict';

const _             = require('lodash');
const env           = require('./' + (process.env.NODE_ENV || 'development'));
const packageJson   = require('../../package.json');

const all           = {
    log : {
        showRouteAtStart : true
    },
    connections : {
        api : {
            host    : 'localhost',
            port    : process.env.PORT || 8080,
            labels  : [ 'api' ]
        },
        mongodb : {
            host : 'localhost',
            port : '27017'
        },
        socket : {
            host : 'localhost',
            port : '8081'
        }
    }
};

module.exports = _.merge(all, env);
