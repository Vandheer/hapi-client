/**
 * Created by Jojo on 24/01/2017.
 */
'use strict';

const fs = require('fs');
const path = require('path');
const modelsDir = path.join(__dirname, '../../app/models/');
const models = fs.readdirSync(modelsDir);
const envConfig = require('../environments/all');

module.exports.init = server => {
    return new Promise((resolve, reject) => {
        server.register({
            register : require('k7'),
            options : {
                connectionString : 'mongodb://' + envConfig.connections.mongodb.host + ':' + envConfig.connections.mongodb.port + '/hapi',
                adapter : require('k7-mongoose'),
                models : [
                    path.join(modelsDir, '**/*.js')
                ],
            }
        }, err => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
};
