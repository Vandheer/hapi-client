'use strict';

const jsonToMongoose    = require('json-mongoose');
const async             = require('async');
const crypto            = require('crypto');
const mongoose          = require('k7-mongoose').mongoose();

module.exports = jsonToMongoose({
    mongoose    : mongoose,
    collection  : 'users',
    schema      : require('../schemas/users'),
    pre         : {
        save : (doc, next) => {
            async.parallel({
                password : done => {
                    doc.password = crypto.createHash('md5').update(doc.password).digest('hex');
                    done();
                }
            }, next);
        }
    },
    schemaUpdate : (schema) => {
        schema.email.unique  = true; // On se base dessus pour identifier les users
        return schema;
    },
    options : {

    }
});
