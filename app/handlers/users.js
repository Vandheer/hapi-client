'use strict';
const Model = require('../models/users');
const crypto = require('crypto');

/**
 * Crée un utilisateur à partir d'un formulaire
 */
module.exports.createUser = (request, response) => {
    let theUser = new Model(request.payload);

    theUser.save(function (err, user) {
        if (err) throw err;

        // On demande à envoyer un mail
        request.server.ioClient.emit('mail-create', {
            user : request.payload
        }, function() {
            console.log('Mail requested');
        });
        // Confirmation par le serveur
        request.server.ioClient.on('ok-mail', () => {
            console.log('Mail successfully sent');
        });
        response(user).code(201);
    });
};

/**
 * Récupère un utilisateur à partir de son email
 */
module.exports.getUser = (request, response) => {
    let email = request.params.email;

    Model.findOne({ 'email': email }, function (err, user) {
        if (err) return err;

        response(null, { result : user }).code(204);
    });
};

/**
 * Modifie les informations d'un utilisateur
 */
module.exports.updateUser = (request, response) => {
    let theUser = new Model(request.payload);

    Model.update({ 'email': theUser.email }, {
        login : theUser.login,
        firstName: theUser.firstName,
        lastName : theUser.lastName,
        company : theUser.company,
        function : theUser.function,
        nir : theUser.nir
    }, function (err, user) {
        if (err) return err;

        if (user != null) {
            // On demande à envoyer un mail
            request.server.ioClient.emit('mail-update', {
                user : request.payload
            }, function() {
                console.log('Mail requested');
            });
            // Message de confirmation
            request.server.ioClient.on('ok-mail', () => {
                console.log('Mail successfully sent');
            });
        }
        response(theUser).code(204);
    });
};

/**
 * Supprime d'un utilisateur à partir de son email
 */
module.exports.deleteUser = (request, response) => {
    let email = request.params.email;

    Model.findOne({ 'email': email }, function (err) {
        if (err) return err;
    }).remove(function (err) {
        if (err) return err;

        response(null).code(204);
    });
};

/**
 * Fait un reset du mot de passe d'un utilisateur, envoyé par mail
 */
module.exports.recoverPassword = (request, response) => {
    let email = request.payload.email;
    let password = randomPassword(8);
    let crypted = encode(password);

    Model.findOneAndUpdate({ 'email': email },{ $set: { password: crypted }},function (err, user) {
        if (err) return err;

        // On demande à envoyer un mail
        request.server.ioClient.emit('mail-recover', {
            user : request.payload,
            password : password
        }, function() {
            console.log('Mail requested');
        });
        // Message de confirmation
        request.server.ioClient.on('ok-mail', () => {
            console.log('Mail successfully sent');
        });
        response(null, { msg: 'Mail envoyé' }).code(200);
    });
};

/**
 * Authentifie un utilisateur
 */
module.exports.authent = (request, response) => {
    let login = request.payload.login;
    let password = request.payload.password;

    Model.findOne({ 'login': login }, function (err, user) {
        if (err) return err;

        let crypted = encode(password);
        if (user.password == crypted)
            response(null, { msg: 'ok' }).code(200);
        else
            response(null, { msg: 'ko' }).code(401);
    });
};

/**
 * Cryptage d'une chaîne de caractères
 * @param password la chaîne à encoder
 */
function encode(password) {
    return crypto.createHash('md5').update(password).digest('hex');
}

/**
 * Génération aléatoire d'une chaîne de caractères
 * @param length la longueur de la chaîne
 */
function randomPassword(length)
{
    if (length > 0) {
        let string = "";
        let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( let i=0; i < length; i++ )
            string += chars.charAt(Math.floor(Math.random() * chars.length));
        return string;
    }
}