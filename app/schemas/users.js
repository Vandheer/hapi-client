'use strict';

const Joi = require('joi');

const user = Joi.object({
    login           : Joi.string().required(),
    password        : Joi.string().required().min(8),
    email           : Joi.string().email().required(),
    firstName       : Joi.string().required(),
    lastName        : Joi.string().required(),
    company         : Joi.string().required(),
    function        : Joi.string().required(),
    nir             : Joi.string().length(15)
});
module.exports = user;