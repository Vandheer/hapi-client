'use strict';
const Joi = require('joi');
const handler = require('../handlers/users');
const user = require('../schemas/users');

exports.register = (server, options, next) => {
    server.route([
        {
            method : 'POST',
            path   : '/user',
            config : {
                description : 'Crée un user',
                handler     : handler.createUser,
                tags: ['api'],
                validate: {
                    payload: user
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method : 'GET',
            path   : '/user/{email}',
            config : {
                description : 'Recupère un user',
                handler     : handler.getUser,
                tags: ['api'],
                validate: {
                    params: {
                        email: Joi.string().required()
                    }
                }
            }
        },
        {
            method : 'PUT',
            path   : '/user/update',
            config : {
                description : 'Modifie un user (sauf l\'email et le mdp)',
                handler     : handler.updateUser,
                tags: ['api'],
                validate: {
                    payload: user
                },
            }
        },
        {
            method : 'GET',
            path   : '/user/delete/{email}',
            config : {
                description : 'Supprime un user',
                handler     : handler.deleteUser,
                tags: ['api'],
                validate: {
                    params: {
                        email: Joi.string().required()
                    }
                }
            }
        },
        {
            method : 'POST',
            path   : '/recover',
            config : {
                description : 'Récupération de mot de passe',
                handler     : handler.recoverPassword,
                tags: ['api'],
                validate: {
                    payload: {
                        email: Joi.string().required()
                    }
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method : 'POST',
            path   : '/authent',
            config : {
                description : 'Authentification',
                handler     : handler.authent,
                tags: ['api'],
                validate: {
                    payload: {
                        login: Joi.string().required(),
                        password: Joi.string().required()
                    }
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        }
    ]);
    next();
};

exports.register.attributes = {
    name : 'users-routes'
};
